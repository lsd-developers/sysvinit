all install clean distclean:
	$(MAKE) -C src $@

dist:
	git archive HEAD --prefix=sysvinit-2.90/ | xz > sysvinit-2.90.tar.xz