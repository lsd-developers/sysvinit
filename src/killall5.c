/*
 * Copyright (C) 1991-2004 Miquel van Smoorenburg.
 * Copyright (C) 2013 Ivailo Monev.
 *
 * This file is part of SysVinit suite.
 * 
 * SysVinit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SysVinit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SysVinit.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:	Miquel van Smoorenburg, miquels@cistron.nl
 *
 *		Riku Meskanen, <mesrik@jyu.fi>
 *		- return all running pids of given program name
 *		- single shot '-s' option for backwards combatibility
 *		- omit pid '-o' option and %PPID (parent pid metavariable)
 *		- syslog() only if not a connected to controlling terminal
 *		- swapped out programs pids are caught now
 *
 *		Werner Fink
 *		- make omit dynamic
 *		- provide '-n' to skip stat(2) syscall on network based FS
 *
 */
#include <dirent.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <mntent.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <syslog.h>
#include <sys/mman.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#ifndef PATH_MAX
#  ifdef MAXPATHLEN
#    define PATH_MAX MAXPATHLEN
#  else
#    define PATH_MAX 2048
#  endif
#endif

#define STATNAMELEN	15
#define DO_NETFS 2
#define DO_STAT 1
#define NO_STAT 0

/* Info about a process. */
typedef struct proc {
	char *pathname;		/* full path to executable        */
	char *argv0;		/* Name as found out from argv[0] */
	char *argv0base;	/* `basename argv[1]`		  */
	char *argv1;		/* Name as found out from argv[1] */
	char *argv1base;	/* `basename argv[1]`		  */
	char *statname;		/* the statname without braces    */
	ino_t ino;		/* Inode number			  */
	dev_t dev;		/* Device it is on		  */
	pid_t pid;		/* Process ID.			  */
	pid_t sid;		/* Session ID.			  */
	char kernel;		/* Kernel thread or zombie.	  */
	char nfs;		/* Name found on network FS.	  */
	struct proc *next;	/* Pointer to next struct. 	  */
} PROC;

/* pid queue */

typedef struct pidq {
	PROC		*proc;
	struct pidq	*next;
} PIDQ;

typedef struct {
	PIDQ		*head;
	PIDQ		*tail;
	PIDQ		*next;
} PIDQ_HEAD;

typedef struct _s_omit {
	struct _s_omit *next;
	struct _s_omit *prev;
	pid_t pid;
} OMIT;

typedef struct _s_shadow
{
	struct _s_shadow *next;
	struct _s_shadow *prev;
	size_t nlen;
	char * name;
} SHADOW;

typedef struct _s_nfs
{
	struct _s_nfs *next;	/* Pointer to next struct. */
	struct _s_nfs *prev;	/* Pointer to previous st. */
	SHADOW *shadow;		/* Pointer to shadows      */
	size_t nlen;
	char * name;
} NFS;

/* List of processes. */
PROC *plist;

/* List of processes to omit. */
OMIT *omit;

/* List of NFS mountes partitions. */
NFS *nlist;

/* Did we stop all processes ? */
int sent_sigstop;

int scripts_too = 0;

#ifdef __GNUC__
__attribute__ ((format (printf, 2, 3)))
#endif
void nsyslog(int pri, char *fmt, ...);

#if !defined(__STDC_VERSION__) || (__STDC_VERSION__ < 199901L)
# ifndef  inline
#  define inline	__inline__
# endif
# ifndef  restrict
#  define restrict	__restrict__
# endif
#endif
#define alignof(type)	((sizeof(type)+(sizeof(void*)-1)) & ~(sizeof(void*)-1))

/*
 *	Malloc space, barf if out of memory.
 */
#ifdef __GNUC__
static void *xmalloc(size_t) __attribute__ ((__malloc__));
#endif
static void *xmalloc(size_t bytes)
{
	void *p;

	if ((p = malloc(bytes)) == NULL) {
		if (sent_sigstop) kill(-1, SIGCONT);
		nsyslog(LOG_ERR, "out of memory");
		exit(1);
	}
	return p;
}

#ifdef __GNUC__
static inline void xmemalign(void **, size_t, size_t) __attribute__ ((__nonnull__ (1)));
#endif
static inline void xmemalign(void **memptr, size_t alignment, size_t size)
{
	if ((posix_memalign(memptr, alignment, size)) < 0) {
		if (sent_sigstop) kill(-1, SIGCONT);
		nsyslog(LOG_ERR, "out of memory");
		exit(1);
	}
}

/*
 *	See if the proc filesystem is there. Mount if needed.
 */
int mount_proc(void)
{
	struct stat	st;
	char		*args[] = { "mount", "-t", "proc", "proc", "/proc", 0 };
	pid_t		pid, rc;
	int		wst;
	int		did_mount = 0;

	/* Stat /proc/version to see if /proc is mounted. */
	if (stat("/proc/version", &st) < 0 && errno == ENOENT) {

		/* It's not there, so mount it. */
		if ((pid = fork()) < 0) {
			nsyslog(LOG_ERR, "cannot fork");
			exit(1);
		}
		if (pid == 0) {
			/* Try a few mount binaries. */
			execv("/bin/mount", args);
			execv("/sbin/mount", args);

			/* Okay, I give up. */
			nsyslog(LOG_ERR, "cannot execute mount");
			exit(1);
		}
		/* Wait for child. */
		while ((rc = wait(&wst)) != pid)
			if (rc < 0 && errno == ECHILD)
				break;
		if (rc != pid || WEXITSTATUS(wst) != 0)
			nsyslog(LOG_ERR, "mount returned non-zero exit status");

		did_mount = 1;
	}

	/* See if mount succeeded. */
	if (stat("/proc/version", &st) < 0) {
		if (errno == ENOENT)
			nsyslog(LOG_ERR, "/proc not mounted, failed to mount.");
		else
			nsyslog(LOG_ERR, "/proc unavailable.");
		exit(1);
	}

	return did_mount;
}

/*
 *     Check if path is a shadow off a NFS partition.
 */
static int shadow(SHADOW *restrict this, const char *restrict name, const size_t nlen)
{
	SHADOW *s;

	if (!this)
		goto out;
	for (s = this; s; s = s->next) {
		if (nlen < s->nlen)
			continue;
		if (name[s->nlen] != '\0' && name[s->nlen] != '/')
			continue;
		if (strncmp(name, s->name, s->nlen) == 0)
			return 1;
	}
out:
	return 0;
}

/*
 *     Check path is located on a network based partition.
 */
int check4nfs(const char * path, char * real)
{
	char buf[PATH_MAX+1];
	const char *curr;
	int deep = MAXSYMLINKS;

	if (!nlist) return 0;

	curr = path;
	do {
		const char *prev;
		int len;

		if ((prev = strdupa(curr)) == NULL) {
			nsyslog(LOG_ERR, "strdupa(): %s\n", strerror(errno));
			return 0;
		}

		errno = 0;
		if ((len = readlink(curr, buf, PATH_MAX)) < 0)
			break;
		buf[len] = '\0';

		if (buf[0] != '/') {
			const char *slash;

			if ((slash = strrchr(prev, '/'))) {
				size_t off = slash - prev + 1;

				if (off + len > PATH_MAX)
					len = PATH_MAX - off;

				memmove(&buf[off], &buf[0], len + 1);
				memcpy(&buf[0], prev, off);
			}
		}
		curr = &buf[0];

		if (deep-- <= 0) return 0;

	} while (1);

	if (real) strcpy(real, curr);

	if (errno == EINVAL) {
		const size_t nlen = strlen(curr);
		NFS *p;
		for (p = nlist; p; p = p->next) {
			if (nlen < p->nlen)
				continue;
			if (curr[p->nlen] != '\0' && curr[p->nlen] != '/')
				continue;
			if (!strncmp(curr, p->name, p->nlen)) {
				if (shadow(p->shadow, curr, nlen))
					continue;
				return 1;
			}
		}
	}

	return 0;
}

int readarg(FILE *fp, char *buf, int sz)
{
	int		c = 0, f = 0;

	while (f < (sz-1) && (c = fgetc(fp)) != EOF && c)
		buf[f++] = c;
	buf[f] = 0;

	return (c == EOF && f == 0) ? c : f;
}

/*
 *	Read the proc filesystem.
 *	CWD must be /proc to avoid problems if / is affected by the killing (ie depend on fuse).
 */
int readproc(int do_stat)
{
	DIR		*dir;
	FILE		*fp;
	PROC		*p, *n;
	struct dirent	*d;
	struct stat	st;
	char		path[PATH_MAX+1];
	char		buf[PATH_MAX+1];
	char		*s, *q;
	unsigned long	startcode, endcode;
	int		pid, f;
	ssize_t		len;

	/* Open the /proc directory. */
	if (chdir("/proc") == -1) {
		nsyslog(LOG_ERR, "chdir /proc failed");
		return -1;
	}
	if ((dir = opendir(".")) == NULL) {
		nsyslog(LOG_ERR, "cannot opendir(/proc)");
		return -1;
	}

	/* Free the already existing process list. */
	n = plist;
	for (p = plist; n; p = n) {
		n = p->next;
		if (p->argv0) free(p->argv0);
		if (p->argv1) free(p->argv1);
		if (p->statname) free(p->statname);
		free(p->pathname);
		free(p);
	}
	plist = NULL;

	/* Walk through the directory. */
	while ((d = readdir(dir)) != NULL) {

		/* See if this is a process */
		if ((pid = atoi(d->d_name)) == 0) continue;

		/* Get a PROC struct . */
		p = (PROC *)xmalloc(sizeof(PROC));
		memset(p, 0, sizeof(PROC));

		/* Open the status file. */
		snprintf(path, sizeof(path), "%s/stat", d->d_name);

		/* Read SID & statname from it. */
		if ((fp = fopen(path, "r")) != NULL) {
			size_t len;

			len = fread(buf, sizeof(char), sizeof(buf)-1, fp);
			buf[len] = '\0';

			if (buf[0] == '\0') {
				nsyslog(LOG_ERR,
					"can't read from %s\n", path);
				fclose(fp);
				free(p);
				continue;
			}

			/* See if name starts with '(' */
			s = buf;
			while (*s && *s != ' ') s++;
			if (*s) s++;
			if (*s == '(') {
				/* Read program name. */
				q = strrchr(buf, ')');
				if (q == NULL) {
					p->sid = 0;
					nsyslog(LOG_ERR,
					"can't get program name from /proc/%s\n",
						path);
					fclose(fp);
					if (p->argv0) free(p->argv0);
					if (p->argv1) free(p->argv1);
					if (p->statname) free(p->statname);
					free(p->pathname);
					free(p);
					continue;
				}
				s++;
			} else {
				q = s;
				while (*q && *q != ' ') q++;
			}
			if (*q) *q++ = 0;
			while (*q == ' ') q++;
			p->statname = (char *)xmalloc(strlen(s)+1);
			strcpy(p->statname, s);

			/* Get session, startcode, endcode. */
			startcode = endcode = 0;
			if (sscanf(q, 	"%*c %*d %*d %d %*d %*d %*u %*u "
					"%*u %*u %*u %*u %*u %*d %*d "
					"%*d %*d %*d %*d %*u %*u %*d "
					"%*u %lu %lu",
					&p->sid, &startcode, &endcode) != 3) {
				p->sid = 0;
				nsyslog(LOG_ERR, "can't read sid from %s\n",
					path);
				fclose(fp);
				if (p->argv0) free(p->argv0);
				if (p->argv1) free(p->argv1);
				if (p->statname) free(p->statname);
				free(p->pathname);
				free(p);
				continue;
			}
			if (startcode == 0 && endcode == 0)
				p->kernel = 1;
			fclose(fp);
		} else {
			/* Process disappeared.. */
			if (p->argv0) free(p->argv0);
			if (p->argv1) free(p->argv1);
			if (p->statname) free(p->statname);
			free(p->pathname);
			free(p);
			continue;
		}

		snprintf(path, sizeof(path), "%s/cmdline", d->d_name);
		if ((fp = fopen(path, "r")) != NULL) {

			/* Now read argv[0] */
			f = readarg(fp, buf, sizeof(buf));

			if (buf[0]) {
				/* Store the name into malloced memory. */
				p->argv0 = (char *)xmalloc(f + 1);
				strcpy(p->argv0, buf);

				/* Get a pointer to the basename. */
				p->argv0base = strrchr(p->argv0, '/');
				if (p->argv0base != NULL)
					p->argv0base++;
				else
					p->argv0base = p->argv0;
			}

			/* And read argv[1] */
			while ((f = readarg(fp, buf, sizeof(buf))) != EOF)
				if (buf[0] != '-') break;

			if (buf[0]) {
				/* Store the name into malloced memory. */
				p->argv1 = (char *)xmalloc(f + 1);
				strcpy(p->argv1, buf);

				/* Get a pointer to the basename. */
				p->argv1base = strrchr(p->argv1, '/');
				if (p->argv1base != NULL)
					p->argv1base++;
				else
					p->argv1base = p->argv1;
			}

			fclose(fp);

		} else {
			/* Process disappeared.. */
			if (p->argv0) free(p->argv0);
			if (p->argv1) free(p->argv1);
			if (p->statname) free(p->statname);
			free(p->pathname);
			free(p);
			continue;
		}

		/* Try to stat the executable. */
		snprintf(path, sizeof(path), "/proc/%s/exe", d->d_name);

		p->nfs = 0;

		switch (do_stat) {
		case DO_NETFS:
			if ((p->nfs = check4nfs(path, buf)))
				goto link;
		case DO_STAT:
			if (stat(path, &st) != 0) {
				char * ptr;

				len = readlink(path, buf, PATH_MAX);
				if (len <= 0)
					break;
				buf[len] = '\0';

				ptr = strstr(buf, " (deleted)");
				if (!ptr)
					break;
				*ptr = '\0';
				len -= strlen(" (deleted)");

				if (stat(buf, &st) != 0)
					break;
				p->dev = st.st_dev;
				p->ino = st.st_ino;
				p->pathname = (char *)xmalloc(len + 1);
				memcpy(p->pathname, buf, len);
				p->pathname[len] = '\0';

				/* All done */
				break;
			}

			p->dev = st.st_dev;
			p->ino = st.st_ino;

			/* Fall through */
		default:
		link:
			len = readlink(path, buf, PATH_MAX);
			if (len > 0) {
				p->pathname = (char *)xmalloc(len + 1);
				memcpy(p->pathname, buf, len);
				p->pathname[len] = '\0';
			}
			break;
		}

		/* Link it into the list. */
		p->next = plist;
		plist = p;
		p->pid = pid;
	}
	closedir(dir);

	/* Done. */
	return 0;
}

/* Give usage message and exit. */
void usage(void)
{
	nsyslog(LOG_ERR, "only one argument, a signal number, allowed");
	closelog();
	exit(1);
}

/* write to syslog file if not open terminal */
#ifdef __GNUC__
__attribute__ ((format (printf, 2, 3)))
#endif
void nsyslog(int pri, char *fmt, ...)
{
	va_list  args;

	va_start(args, fmt);

	if (ttyname(0) == NULL) {
		vsyslog(pri, fmt, args);
	} else {
		fprintf(stderr, "killall5: ");
		vfprintf(stderr, fmt, args);
		fprintf(stderr, "\n");
	}

	va_end(args);
}

/* Main for killall. */
int main(int argc, char **argv)
{
	PROC		*p;
	int		pid, sid = -1;
	int		sig = SIGKILL;
	int		c;

	/* return non-zero if no process was killed */
	int		retval = 2;

	/* Now connect to syslog. */
	openlog("killall5", LOG_CONS|LOG_PID, LOG_DAEMON);

	omit = (OMIT*)0;

	if (argc > 1) {
		for (c = 1; c < argc; c++) {
			if (argv[c][0] == '-') (argv[c])++;
			if (argv[c][0] == 'o') {
				char * token, * here;

				if (++c >= argc)
					usage();

				here = argv[c];
				while ((token = strsep(&here, ",;:"))) {
					OMIT *restrict optr;
					pid_t opid = (pid_t)atoi(token);

					if (opid < 1) {
						nsyslog(LOG_ERR,
							"illegal omit pid value "
							"(%s)!\n", token);
						continue;
					}
					xmemalign((void*)&optr, sizeof(void*), alignof(OMIT));
					optr->next = omit;
					optr->prev = (OMIT*)0;
					optr->pid  = opid;
					omit = optr;
				}
			}
			else if ((sig = atoi(argv[1])) <= 0 || sig > 31)
				usage();
		}
	}

	/* First get the /proc filesystem online. */
	mount_proc();

	/*
	 *	Ignoring SIGKILL and SIGSTOP do not make sense, but
	 *	someday kill(-1, sig) might kill ourself if we don't
	 *	do this. This certainly is a valid concern for SIGTERM-
	 *	Linux 2.1 might send the calling process the signal too.
	 */
	signal(SIGTERM, SIG_IGN);
	signal(SIGSTOP, SIG_IGN);
	signal(SIGKILL, SIG_IGN);

	/* lock us into memory */
	mlockall(MCL_CURRENT | MCL_FUTURE);

	/* Now stop all processes. */
	kill(-1, SIGSTOP);
	sent_sigstop = 1;

	/* Read /proc filesystem */
	if (readproc(NO_STAT) < 0) {
		kill(-1, SIGCONT);
		return(1);
	}

	/* Now kill all processes except init (pid 1) and our session. */
	sid = (int)getsid(0);
	pid = (int)getpid();
	for (p = plist; p; p = p->next) {
		if (p->pid == 1 || p->pid == pid || p->sid == sid || p->kernel)
			continue;

		if (omit) {
			OMIT * optr;
			for (optr = omit; optr; optr = optr->next) {
				if (optr->pid == p->pid)
					break;
			}

			/* On a match, continue with the for loop above. */
			if (optr)
				continue;
		}

		kill(p->pid, sig);
		retval = 0;
	}

	/* And let them continue. */
	kill(-1, SIGCONT);

	/* Done. */
	closelog();

	/* Force the kernel to run the scheduler */
	usleep(1);

	return retval;
}
