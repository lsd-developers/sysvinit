/*
 * Copyright (C) 2005 Miquel van Smoorenburg.
 * Copyright (C) 2013 Ivailo Monev.
 *
 * This file is part of SysVinit suite.
 * 
 * SysVinit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SysVinit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SysVinit.  If not, see <http://www.gnu.org/licenses/>.
 */

#define ISMEMBER(set, val) ((set) & (1 << (val)))
#define DELSET(set, val)   ((set) &= ~(1 << (val)))
#define ADDSET(set, val)   ((set) |=  (1 << (val)))
#define EMPTYSET(set)      ((set) = 0)

