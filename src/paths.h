/*
 * Copyright (C) 1991-2001 Miquel van Smoorenburg.
 * Copyright (C) 2013 Ivailo Monev.
 *
 * This file is part of SysVinit suite.
 * 
 * SysVinit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SysVinit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SysVinit.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author:	Miquel van Smoorenburg, <miquels@cistron.nl>
 *
 */
 
#define VT_MASTER	"/dev/tty0"		/* Virtual console master */
#define CONSOLE		"/dev/console"		/* Logical system console */
#define SECURETTY	"/etc/securetty"	/* List of root terminals */
#define SDALLOW		"/etc/shutdown.allow"	/* Users allowed to shutdown */
#define INITTAB		"/etc/inittab"		/* Location of inittab */
#define INIT		"/sbin/init"		/* Location of init itself. */
#define NOLOGIN		"/etc/nologin"		/* Stop user logging in. */
#define FASTBOOT	"/fastboot"		/* Enable fast boot. */
#define FORCEFSCK	"/forcefsck"		/* Force fsck on boot */
#define SDPID		"/var/run/shutdown.pid"	/* PID of shutdown program */
#define SHELL		"/bin/sh"		/* Default shell */
#define SULOGIN		"/sbin/sulogin"		/* Sulogin */
#define INITSCRIPT	"/etc/initscript"	/* Initscript. */
#define PWRSTAT_OLD	"/etc/powerstatus"	/* COMPAT: SIGPWR reason (OK/BAD) */
#define PWRSTAT		"/var/run/powerstatus"	/* COMPAT: SIGPWR reason (OK/BAD) */
